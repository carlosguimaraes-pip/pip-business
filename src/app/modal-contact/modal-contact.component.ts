import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { SendEmailService } from '../send-email.service';

@Component({
	selector: 'app-modal-contact',
	templateUrl: './modal-contact.component.html',
	styleUrls: ['./modal-contact.component.scss']
})
export class ModalContactComponent extends DialogComponent<any, boolean> implements OnInit {

	contact: any = {
		name: '', email: '',
		phone: '', subject: '',
		html: '', text: ''
	};

	constructor(dialogService: DialogService,
				private emailServ: SendEmailService) {
		super(dialogService);
	}

	ngOnInit() { }

	closeDialoag(): void {
		this.close();
	}

	sendEmail(): void {
		if(this.validadeFields()) {
			if(this.validateEmail(this.contact.email)) {
				this.contact.html = this.formatText();
				this.emailServ.sendEmail(this.contact);
			} else {
				this.focusOnField('email', false);
			}
		}
	}
	

	validadeFields(): boolean {
		if (this.contact.text == '') {
			this.focusOnField('msg', false);
		} else {
			this.focusOnField('msg', true);
		}

		if (this.contact.subject == '') {
			this.focusOnField('subject', false);
		} else {
			this.focusOnField('subject', true);
		}

		if (this.contact.email == '') {
			this.focusOnField('email', false);
		} else {
			this.focusOnField('email', true);
		}

		if(this.contact.name == '') {
			this.focusOnField('name', false);
		} else {
			this.focusOnField('name', true);
		}

		if (this.contact.name != '' && this.contact.email != '' && this.contact.subject != '' && this.contact.text != '' ) {
			return true;
		} else {
			return false;
		}
	}

	validateEmail(email: string) {
        const expression = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return expression.test(email);
    }

	focusOnField(inputId: string, wrong: boolean): void {
		if(!wrong) {
			document.getElementById(inputId).focus();
			document.getElementById(inputId).style.borderColor = 'red';
		} else {
			document.getElementById(inputId).style.borderColor = '#dedede';
		}
	}

	formatText(): string {
		let text = '';

		if(this.contact.text != '' && this.contact.phone != '') {
			text = this.contact.text +'<br> <br> Telefone para contato: <b>'+ this.contact.phone + '</b>';
			return text;

		} else if(this.contact.text != '') {
			return this.contact.text;

		} else if(this.contact.phone != '') {
			return 'Telefone para contato: ' + this.contact.phone;
		}
	}

}

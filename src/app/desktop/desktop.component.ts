import { Component, OnInit } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { DialogService } from 'ng2-bootstrap-modal';
import { ModalContactComponent } from '../modal-contact/modal-contact.component';
import { ModalSubscriptionComponent } from '../modal-subscription/modal-subscription.component';
import { SendEmailService } from '../send-email.service';

@Component({
	selector: 'app-desktop',
	templateUrl: './desktop.component.html',
	styleUrls: ['./desktop.component.scss']
})
export class DesktopComponent implements OnInit {

	timeline: boolean = true;
	recipe: boolean = false;
	book: boolean = false;
	collection: boolean = false;
	currentTab: string = 'timeline';
	carouselOne: NgxCarousel;

	titleEmail: string;
	infoEmail: any = {};

	constructor(private dialogService: DialogService,
				private emailServ: SendEmailService) {
	}

	ngOnInit(): void {
		document.getElementById('timeline').className = 'nav-item-about-selected';
		document.getElementById('recipes').className = 'nav-item-about';
		document.getElementById('book').className = 'nav-item-about';
		document.getElementById('collections').className = 'nav-item-about';
		this.carouselOne = {
			grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
			slide: 1,
			speed: 400,
			interval: 4000,
			point: {
				visible: true
			},
			load: 2,
			touch: true,
			loop: true,
			custom: 'banner'
		}
	}

	changeNavOp(type: string): void {
		this.timeline = false;
		this.recipe = false;
		this.book = false;
		this.collection = false;
		document.getElementById(this.currentTab).className = 'nav-item-about';
		this.currentTab = type;
		if (type == 'timeline') {
			document.getElementById('timeline').className = 'nav-item-about-selected';
			this.timeline = true;
		} else if (type == 'recipes') {
			document.getElementById('recipes').className = 'nav-item-about-selected';
			this.recipe = true;
		} else if (type == 'collections') {
			document.getElementById('collections').className = 'nav-item-about-selected';
			this.collection = true;
		} else if (type == 'book') {
			document.getElementById('book').className = 'nav-item-about-selected';
			this.book = true;
		}
	}

	openDialog(): void {
		this.dialogService.addDialog(ModalContactComponent)
			.subscribe((resp) => {
				console.log(resp);
			});
	}

	openSignature(): void {
		this.dialogService.addDialog(ModalSubscriptionComponent)
			.subscribe((resp) => {
				console.log(resp);
			});
	}

	sendEmail(): void {
		if(this.validateEmail(this.titleEmail)) {
			this.infoEmail = {
				email: this.titleEmail,
				subject: 'Mais informações sobre PIP FOR BUSINESS',
				html: 'Usuário: <b>'+this.titleEmail+'</b> gostaria de saber mais sobre o <b> pip for business </b>'
			}
			this.emailServ.sendEmailInfo(this.infoEmail);
		}

	}

	validateEmail(email: string) {
        const expression = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return expression.test(email);
    }

}

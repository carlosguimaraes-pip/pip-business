import { Component } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { DialogService } from 'ng2-bootstrap-modal';
import { ModalContactComponent } from './modal-contact/modal-contact.component';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'app';
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxCarouselModule } from 'ngx-carousel';
import { AppComponent } from './app.component';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import 'hammerjs';
import { CoreModule } from "./core/core.module";
import { ModalContactComponent } from './modal-contact/modal-contact.component';
import { ModalSubscriptionComponent } from "./modal-subscription/modal-subscription.component";
import { DesktopComponent } from './desktop/desktop.component';
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { MobileComponent } from './mobile/mobile.component';
import { PhoneComponent } from './phone/phone.component';
import { SendEmailService } from './send-email.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ModalContactComponent,
    ModalSubscriptionComponent,
    DesktopComponent,
    MobileComponent,
    PhoneComponent
  ],
  imports: [
    BrowserModule,
    NgxCarouselModule,
    BootstrapModalModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    CoreModule,
    FormsModule
  ],
  providers: [
    SendEmailService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalContactComponent,
    ModalSubscriptionComponent
  ]
})
export class AppModule { }
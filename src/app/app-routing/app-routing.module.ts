import { NgModule } from '@angular/core';
import { extract } from '../core/i18n.service';
import { Route } from '../core/route.service';
import { RouterModule, Routes } from '@angular/router';

import { DesktopComponent } from "../desktop/desktop.component";
import { MobileComponent } from "../mobile/mobile.component";
import { PhoneComponent } from "../phone/phone.component";
import { SendEmailService } from "../send-email.service";

const routes: Routes = Route.withShell([{
	path: '',
	redirectTo: 'desktop',
	pathMatch: 'full'
}, {
	path: 'desktop',
	component: DesktopComponent,
	data: { title: extract('desktop') }
}, {
	path: 'mobile',
	component: MobileComponent,
	data: { title: extract('mobile') }
}, {
	path: 'phone',
	component: PhoneComponent,
	data: { title: extract('phone') }
}]);

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	],
	declarations: []
})

export class AppRoutingModule { }



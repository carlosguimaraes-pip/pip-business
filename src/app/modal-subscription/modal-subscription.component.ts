import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { SendEmailService } from '../send-email.service';

@Component({
	selector: 'app-modal-subscription',
	templateUrl: './modal-subscription.component.html',
	styleUrls: ['./modal-subscription.component.scss']
})
export class ModalSubscriptionComponent extends DialogComponent<any, boolean> implements OnInit {

	subscription: any = {
		name: '', email: '',
		phone: '', plan: '0',
		company: '', subject: '',
		context: ''

	};

	constructor(dialogService: DialogService,
				private emailServ: SendEmailService) {
		super(dialogService);
	}

	ngOnInit() {
	}

	closeDialoag(): void {
		this.close();
	}

	sendEmail(): void {
		if(this.validadeFields()) {
			if(this.validateEmail(this.subscription.email)) {
				this.subscription.subject = this.formatSubject();
				this.subscription.context = this.formatContext();
				this.emailServ.sendEmailSub(this.subscription);
			} else {
				this.focusOnField('email', false);
			}
		}
	}

	validadeFields(): boolean {
		if(this.subscription.plan == '0') {
			this.focusOnField('plan', false);
		} else {
			this.focusOnField('plan', true);
		}

		if (this.subscription.email == '') {
			this.focusOnField('email', false);
		} else {
			this.focusOnField('email', true);
		}

		if(this.subscription.company == '') {
			this.focusOnField('company', false);
		} else {
			this.focusOnField('company', true);
		}

		if(this.subscription.name == '') {
			this.focusOnField('name', false);
		} else {
			this.focusOnField('name', true);
		}

		if (this.subscription.name != '' && this.subscription.email != '' && this.subscription.plan != '0' && this.subscription.plan != '') {
			return true;
		} else {
			return false;
		}
	}

	validateEmail(email: string) {
        const expression = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return expression.test(email);
    }

	focusOnField(inputId: string, wrong: boolean): void {
		if(!wrong) {
			document.getElementById(inputId).focus();
			document.getElementById(inputId).style.borderColor = 'red';
		} else {
			document.getElementById(inputId).style.borderColor = '#dedede';
		}
	}

	formatSubject(): string {
		let text = '';
		let plan = '';

		if(this.subscription.plan == 'B') {
			plan = 'Plan Basic';
		}	
		if(this.subscription.plan == 'S') {
			plan = 'Plan Standard';
		}
		if(this.subscription.plan == 'P') {
			plan = 'Plan Premium';
		}
		
		return text = 'Pedido de assinatura '+plan+' por '+ this.subscription.name;
	}

	formatContext(): string {
		let text = '';
		let plan = '';
		let phone = '';

		if(this.subscription.phone != '') {
			phone = 'Telefone para contato: '+this.subscription.phone;
		}
		if(this.subscription.plan == 'B') {
			plan = 'Basic';
		}
		if(this.subscription.plan == 'S') {
			plan = 'Standard';
		}
		if(this.subscription.plan == 'P') {
			plan = 'Premium';
		}

		return text = 'Cliente <b>'+this.subscription.name+'</b>, da empresa <b>'+ this.subscription.company+'</b>, enviou pedido de assinatura do Plano '+ plan+'<br>'+phone;
	}
}

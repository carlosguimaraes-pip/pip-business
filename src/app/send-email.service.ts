import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions } from '@angular/http';

@Injectable()
export class SendEmailService {
	constructor(public http: Http,
				private requestOptions: RequestOptions) { }

	emailEndpoint: string = 'mailer'
	receiver: string = 'guido@piprecipes.com';

	sendEmail(options: any) {
		const mailOptions = {
            from: options.name + ' <' + options.email + '>',
            to: [this.receiver],
            subject: options.subject,
			html: options.html
		};

		this.http.post('http://localhost:3000/mailerLanding', mailOptions ).subscribe( res => { return res });
	}

	sendEmailSub(options: any) {
		const mailOptions = {
            from: options.name + ' <' + options.email + '>',
            to: [this.receiver],
            subject: options.subject,
			html: options.context
		};

		this.http.post('http://localhost:3000/mailerLanding', mailOptions ).subscribe( res => { return res });
	}

	sendEmailInfo(options: any) {
		const mailOptions = {
            from: 'Usuário <' + options.email + '>',
            to: [this.receiver],
            subject: options.subject,
			html: options.html
		};

		this.http.post('http://localhost:3000/mailerLanding', mailOptions ).subscribe( res => { return res });
	}
}
